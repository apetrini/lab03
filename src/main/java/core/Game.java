package Game;

import java.io.*;

public class Game {

	int[] rolls;
	int currentRoll;


	public Game() {
		rolls = new int[21];
		currentRoll = 0;
	}

	public void roll( int pins ) {
		rolls[currentRoll++] = pins;
	}

	public int score() {
		int score = 0;
		int frame = 0;

		for (int i = 0; i < 10; i++) {
			if (rolls[frame] == 10) {
				// Stirke
				score += 10 + rolls[frame + 1] + rolls[frame + 2];
				frame++;
			} else if (rolls[frame] + rolls[frame + 1] == 10) {
				// Spare
				score += 10 + rolls[frame + 2];
				frame += 2;
			} else {
				// no bonus
				score += rolls[frame] + rolls[frame + 1];
				frame += 2;
			}
		}
		return score;
	}
}
